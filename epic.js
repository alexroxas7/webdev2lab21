/**
 * @author Alexander Roxas
 * 
 * @description This script fetches and display images from NASA based on the user inputs.
 * It allows users to select the type of image and the date the image was take to be displayed.
 * 
 * @version 2023-11-16
 */
'use strict';

document.addEventListener("DOMContentLoaded", function(){

    let typesOfImage = ["natural", "enhanced", "aerosol", "cloud"];

    const EpicApplication = {
        imageCache: {},
        imageType: null,
        dates: null,

        dateCache : {
            [typesOfImage[0]]: null,
            [typesOfImage[1]]: null,
            [typesOfImage[2]]: null,
            [typesOfImage[3]]: null
        }
    }
    
    const typeImageInput = document.getElementById("type");

    for (let index = 0; index < typesOfImage.length; ++index){
        const option = document.createElement("option");
        option.value = typesOfImage[index];
        option.textContent = `${typesOfImage[index][0].toUpperCase()}${typesOfImage[index].slice(1)}`;
        typeImageInput.appendChild(option);
    }
    
    /**
     * Initializes different variables from the form element for selecting
     * @type {HTMLSelectElement} = typeImageInput
     * @type {HTMLSelectElement} = imageDateInput
     * @type {HTMLFormElement} = form
     * @type {HTMLUListElement} = ul
     */
    
    const form = document.querySelector("#request_form");
    EpicApplication.imageType = document.getElementById("type");
    EpicApplication.dates = document.getElementById("date");
    const ul = document.querySelector("#image-menu");
    //Calling the dateMax function to set the max date for the ImageDateInput
    dateMax();
    form.addEventListener("submit", function(e){
        e.preventDefault();

        fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.imageType.value}/date/${EpicApplication.dates.value}`)
            .then( response => { 
                if( !response.ok ) { 
                    throw new Error("Not 2xx response", {cause: response});
                }
                return response.json();
            })
            .then( obj => {
                //Clear the list
                ul.textContent = undefined;

                //Populate the list menu with fetched dates.
                let index = 0;
                obj.forEach((element) => {
                    const li = document.querySelector("#image-menu-item").content.cloneNode(true).children[0];
                    ul.appendChild(li);
                    li.firstChild.textContent = element.date;
                    li.firstChild.setAttribute("data-index-of-my-image", index);
                    index++
                });

                if(EpicApplication.imageCache[EpicApplication.imageType.value] !== null){
                    EpicApplication.imageCache[EpicApplication.imageType.value] = new Map();
                  }

                 EpicApplication.imageCache[EpicApplication.imageType.value].set(EpicApplication.dates.value, obj);
            
            })
            .catch( err => {
                console.error("3)Error:", err);
            });  
    });

    /**
     * Event listener that checks for a click on the list and changes the image and footer to the corresponding date on the list.
     * 
     * @param {Event} e 
     */
    ul.addEventListener("click", function(e){
        e.preventDefault();
        if (e.target.tagName !== "SPAN"){
            return;
        }
        let dateArr = EpicApplication.dates.value.split("-");
        let imageIndex = e.target.getAttribute("data-index-of-my-image");
        let obj = EpicApplication.imageCache[EpicApplication.imageType.value].get(EpicApplication.dates.value);
        let image = document.getElementById("earth-image");
        image.src = `https://epic.gsfc.nasa.gov/archive/${EpicApplication.imageType.value}/${dateArr[0]}/${dateArr[1]}/${dateArr[2]}/jpg/${obj[imageIndex].image}.jpg`;

        const footerImageDate = document.getElementById("earth-image-date");
        footerImageDate.textContent = obj[imageIndex].date;
        const footerImageTitle = document.getElementById("earth-image-title");
        footerImageTitle.textContent = obj[imageIndex].caption;  
    });

    /**
     * Function that changes the max date of the DateInput corresponding to the image types on the dropdown.
     */
    function dateMax() {
        const inputValue = document.getElementById("type").value;
        fetch(`https://epic.gsfc.nasa.gov/api/${inputValue}/all`, {})
            .then( response => { 
                if( !response.ok ) { 
                    throw new Error("Not 2xx response", {cause: response});
                }
                return response.json();
            })
            .then( obj => {
                obj.sort((a, b) => b - a);
                EpicApplication.dates.setAttribute("max", obj[0].date);
                EpicApplication.dates.value = obj[0].date;
            })
            .catch( err => {
                console.error("3)Error:", err);
            });
    }
    /**
     * Event listener for a change event from the input dropdown on Image Input;
     * Calls the dateMax function on change.
     * Clears the date list menu.
     */
    typeImageInput.addEventListener("change", function(){
        dateMax();
        ul.textContent = undefined;
    });
});